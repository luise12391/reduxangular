import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Resultados } from 'src/app/models/resultados.model';
import { AppState } from 'src/app/redux/app.reducers';
import { ElementosActions } from 'src/app/redux/elementos/actions';

@Component({
  selector: 'app-tercercuadro',
  templateUrl: './tercercuadro.component.html',
  styleUrls: ['./tercercuadro.component.css']
})
export class TercercuadroComponent implements OnInit {

  numero1: number;
  numero2: number;
  multiplicacion: number;
  constructor(private store: Store<AppState>) {
    this.store.select(state => state.elementos.numeros)
    .subscribe(n => {
      if (n != null) {
        this.numero1 = n.numero1;
        this.numero2 = n.numero2;
        this.multiplicar();
      }
    });

    this.store.select(state => state.elementos.resultados)
    .subscribe(r => {
      if (r != null) {
        this.multiplicacion = r.resta;
      }
    });
  }
  multiplicar() {
    var resultados = new Resultados();
    resultados.multiplicacion = this.numero1 * this.numero2;
    if(this.multiplicacion != resultados.multiplicacion){
      this.store.dispatch(new ElementosActions.MultiplicaElementosAction(resultados));
    }
  }

  ngOnInit(): void {
  }

}



