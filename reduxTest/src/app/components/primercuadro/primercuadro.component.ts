import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Resultados } from 'src/app/models/resultados.model';
import { AppState } from 'src/app/redux/app.reducers';
import { ElementosActions } from 'src/app/redux/elementos/actions';

@Component({
  selector: 'app-primercuadro',
  templateUrl: './primercuadro.component.html',
  styleUrls: ['./primercuadro.component.css']
})
export class PrimercuadroComponent implements OnInit {
  numero1: number;
  numero2: number;
  suma: number;
  constructor(private store: Store<AppState>) {

    this.store.select(state => state.elementos)
    .subscribe(e => {
      if (e.numeros != null) {
        this.numero1 = e.numeros.numero1;
        this.numero2 = e.numeros.numero2;
        this.sumar();
      }
      if (e.resultados != null) {
        this.suma = e.resultados.suma;
      }
    });
  }

  ngOnInit(): void {
  }

  sumar(){
    var resultados = new Resultados();
    resultados.suma = this.numero1 + this.numero2;
    if(this.suma != resultados.suma){
      this.store.dispatch(new ElementosActions.SumaElementosAction(resultados));
    }
  }
}
