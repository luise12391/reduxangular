import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Resultados } from 'src/app/models/resultados.model';
import { AppState } from 'src/app/redux/app.reducers';
import { ElementosActions } from 'src/app/redux/elementos/actions';

@Component({
  selector: 'app-segundocuadro',
  templateUrl: './segundocuadro.component.html',
  styleUrls: ['./segundocuadro.component.css']
})
export class SegundocuadroComponent implements OnInit {
  numero1: number;
  numero2: number;
  resta: number;
  constructor(private store: Store<AppState>) {
    this.store.select(state => state.elementos.numeros)
    .subscribe(n => {
      if (n != null) {
        this.numero1 = n.numero1;
        this.numero2 = n.numero2;
        this.restar();
      }
    });

    this.store.select(state => state.elementos.resultados)
    .subscribe(r => {
      if (r != null) {
        this.resta = r.resta;
      }
    });
  }

  restar() {
    var resultados = new Resultados();
    resultados.resta = this.numero1 - this.numero2;
    if(this.resta != resultados.resta){
      this.store.dispatch(new ElementosActions.RestaElementosAction(resultados));
    }
  }

  ngOnInit(): void {
  }

}
