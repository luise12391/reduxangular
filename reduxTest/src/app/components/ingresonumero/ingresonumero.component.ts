import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Elementos } from 'src/app/models/elementos.model';
import { Resultados } from 'src/app/models/resultados.model';
import { AppState } from 'src/app/redux/app.reducers';
import { InicializaElementosAction } from 'src/app/redux/elementos/actions/elementos.action';
import { Subscription } from 'rxjs';
import { ElementosActions } from 'src/app/redux/elementos/actions';

@Component({
  selector: 'app-ingresonumero',
  templateUrl: './ingresonumero.component.html',
  styleUrls: ['./ingresonumero.component.css']
})
export class IngresonumeroComponent implements OnInit, OnDestroy {
  numero1: number;
  numero2: number;
  subscription: Subscription;

  constructor(private store: Store<AppState>) {
      var elementos = new Elementos();
      elementos.numero1 = 0;
      elementos.numero2 = 0;

      var resultados = new Resultados();
      resultados.suma = 0;
      resultados.resta = 0;
      resultados.multiplicacion = 0;
      resultados.division = 0;

      this.subscription = this.store.select(state => state.elementos.numeros)
      .subscribe(d => {
        if (d != null) {
          if (d.numero1 != this.numero1){
            this.numero1 = d.numero1;
          }

          if (d.numero2 != this.numero2){
            this.numero2 = d.numero2;
          }
        }
      });

      this.store.dispatch(new InicializaElementosAction(elementos, resultados));
   }

  ngOnInit(): void {
  }

  operaciones(){
    var elementos = new Elementos();
    elementos.numero1 = Number(this.numero1);
    elementos.numero2 = Number(this.numero2);
    this.store.dispatch(new ElementosActions.ActualizarElementosAction(elementos));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
