import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresonumeroComponent } from './ingresonumero.component';

describe('IngresonumeroComponent', () => {
  let component: IngresonumeroComponent;
  let fixture: ComponentFixture<IngresonumeroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngresonumeroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresonumeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
