import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/redux/app.reducers';

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.css']
})
export class ResultadosComponent implements OnInit {
  suma: number;
  resta: number;
  multiplicacion: number;

  constructor(private store: Store<AppState>) {

    this.store.select(state => state.elementos.resultados)
    .subscribe(r => {
      if (r != null) {
        this.suma = r.suma;
        this.resta = r.resta;
        this.multiplicacion = r.multiplicacion
      }
    });
  }

  ngOnInit(): void {
  }

}
