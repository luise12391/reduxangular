import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuartocuadroComponent } from './cuartocuadro.component';

describe('CuartocuadroComponent', () => {
  let component: CuartocuadroComponent;
  let fixture: ComponentFixture<CuartocuadroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CuartocuadroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CuartocuadroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
