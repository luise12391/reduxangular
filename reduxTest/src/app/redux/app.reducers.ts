import {
  ActionReducerMap,
  createSelector,
  createFeatureSelector,
  ActionReducer,
  MetaReducer,
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { Elementos } from '../models/elementos.model';
import { Resultados } from '../models/resultados.model';
import { ElementosState, reducerElementos } from './elementos/reducers/elementos.reducer';


export interface AppState {
  elementos: ElementosState;
};

export const appReducers: ActionReducerMap<AppState> = {
  elementos: reducerElementos
};
