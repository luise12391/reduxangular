import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// NgRx
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store} from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects';
//import { StoreRouterConnectingModule } from '@ngrx/router-store'; // opcional
import { StoreDevtoolsModule } from '@ngrx/store-devtools'; // opcional: instalar plugin chrome.

import { appReducers } from './app.reducers';

// Environment
import { environment } from '../../environments/environment';
import { intializeElementostate } from './elementos/reducers/elementos.reducer';

let reducersInitialState = {
  elementos: intializeElementostate()
};

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgRxStoreModule.forRoot(appReducers, { initialState: reducersInitialState}),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    EffectsModule.forRoot([]),
  ]
})
export class ReduxModule { }

