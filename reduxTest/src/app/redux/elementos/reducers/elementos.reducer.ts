import { ElementosActions } from '../actions';
import { Elementos } from '../../../models/elementos.model';
import { Resultados } from '../../../models/resultados.model';
import { ElementosActionTypes, InicializaElementosAction } from '../actions/elementos.action';

export interface ElementosState{
  numeros: Elementos;
  resultados: Resultados;
}

export function intializeElementostate() {
  return {
    numeros: new Elementos(),
    resultados: new Resultados(),
  };
}
  //REDUCERS
export function reducerElementos(
	state:ElementosState,
	action:ElementosActions.ElementosActionsAllTypes
) : ElementosState {
	switch (action.type) {
    case ElementosActionTypes.INICIALIZA_ELEMENTOS:{
      return {
        numeros : Object.assign({}, (action as InicializaElementosAction).elementos),
        resultados : Object.assign({}, (action as InicializaElementosAction).resultados)
      };
    }
    case ElementosActionTypes.ACTUALIZAR_ELEMENTOS:{
      return {
        ...state,
        numeros : Object.assign({}, (action as InicializaElementosAction).elementos)
      };
    }
    case ElementosActionTypes.SUMA_ELEMENTOS:{
      return {
        ...state,
        resultados : {...state.resultados, suma: (action as InicializaElementosAction).resultados.suma }
      };
    }
    case ElementosActionTypes.RESTA_ELEMENTOS:{
      return {
        ...state,
        resultados : {...state.resultados, resta: (action as InicializaElementosAction).resultados.resta }
      };
    }
    case ElementosActionTypes.MULTIPLICA_ELEMENTOS:{
      return {
        ...state,
        resultados : {...state.resultados, multiplicacion: (action as InicializaElementosAction).resultados.multiplicacion }
      };
    }
	}
	return state;
}

