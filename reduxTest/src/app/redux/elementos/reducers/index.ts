import * as appReducer from '../../app.reducers';
import { createSelector, ActionReducerMap, createFeatureSelector } from '@ngrx/store';


// se tiene que exportar así para poder hacer ng build --prod.
import * as fromElementos from './elementos.reducer';


export interface ElementosState {
    elementos: fromElementos.ElementosState;
}fromElementos

export interface State extends appReducer.AppState {
    ELEMENTOS: ElementosState;
}

export const elementosReducers: ActionReducerMap<ElementosState> = {
    elementos: fromElementos.reducerElementos
};

export const selectElementosState = createFeatureSelector<State, ElementosState>('ELEMENTOS');


export const selectElemenots = createSelector(
  selectElementosState,
    (state: ElementosState) => state.elementos
);
