import { Action } from '@ngrx/store';
import { Resultados } from 'src/app/models/resultados.model';
import { Elementos } from '../../../models/elementos.model';

export enum ElementosActionTypes {
  INICIALIZA_ELEMENTOS = '[Elementos] Inicializa los elementos',
  ACTUALIZAR_ELEMENTOS = '[Elementos] Actuzalizar los elementos',
  SUMA_ELEMENTOS = '[Elementos] Suma los elementos',
  RESTA_ELEMENTOS = '[Elementos] Resta los elementos',
  DIVIDE_ELEMENTOS = '[Elementos] Divide los elementos',
  MULTIPLICA_ELEMENTOS = '[Elementos] Multiplica los elementos'
}

export class InicializaElementosAction implements Action {
  type = ElementosActionTypes.INICIALIZA_ELEMENTOS;
  constructor(public elementos: Elementos, public resultados: Resultados) {}
}

export class ActualizarElementosAction implements Action {
  type = ElementosActionTypes.ACTUALIZAR_ELEMENTOS;
  constructor(public elementos: Elementos) {}
}

export class SumaElementosAction implements Action {
  type = ElementosActionTypes.SUMA_ELEMENTOS;
  constructor(public resultados:Resultados) {}
}

export class RestaElementosAction implements Action {
  type = ElementosActionTypes.RESTA_ELEMENTOS;
  constructor(public resultados:Resultados) {}
}

export class DivideElementosAction implements Action {
  type = ElementosActionTypes.DIVIDE_ELEMENTOS;
  constructor(public elementos: Elementos) {}
}

export class MultiplicaElementosAction implements Action {
  type = ElementosActionTypes.MULTIPLICA_ELEMENTOS;
  constructor(public resultados:Resultados) {}
}

export type ElementosActionsAllTypes = InicializaElementosAction |
                                ActualizarElementosAction |
                                SumaElementosAction |
                                RestaElementosAction|
                                DivideElementosAction |
                                MultiplicaElementosAction;
