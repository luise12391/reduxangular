import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PrimercuadroComponent } from './components/primercuadro/primercuadro.component';
import { SegundocuadroComponent } from './components/segundocuadro/segundocuadro.component';
import { TercercuadroComponent } from './components/tercercuadro/tercercuadro.component';
import { CuartocuadroComponent } from './components/cuartocuadro/cuartocuadro.component';
import { IngresonumeroComponent } from './components/ingresonumero/ingresonumero.component';
import { ReduxModule } from './redux/redux.module';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { intializeElementostate, reducerElementos } from './redux/elementos/reducers/elementos.reducer';
import { elementosReducers } from './redux/elementos/reducers';
import { ResultadosComponent } from './components/resultados/resultados.component';

@NgModule({
  declarations: [AppComponent,
    PrimercuadroComponent,
    SegundocuadroComponent,
    TercercuadroComponent,
    CuartocuadroComponent,
    IngresonumeroComponent,
    ResultadosComponent],
  imports: [
    BrowserModule,
    ReduxModule,
    FormsModule,
    //StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    //StoreModule.forFeature('ELEMENTOS', elementosReducers),
    //EffectsModule.forFeature([])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
